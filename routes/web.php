<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return redirect(\route('get-posts'));
});

# posts
Route::middleware('auth')->group(function () {
    Route::get('/posts/create', 'PostController@createView')->name('get-create-post');
    Route::get('/posts/{id}/edit', 'PostController@updateView')->name('get-update-post');
    Route::post('/posts', 'PostController@create')->name('post-post');
    Route::delete('/posts/{id}', 'PostController@delete')->name('delete-post');
    Route::patch('/posts/{id}', 'PostController@update')->name('patch-post');
});

Route::get('/posts', 'PostController@index')->name('get-posts');
Route::get('/posts/{id}', 'PostController@view')->name('get-post');


# comments
Route::post('/comments', 'CommentController@create')->name('post-comment');
