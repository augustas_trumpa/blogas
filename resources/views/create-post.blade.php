@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <h1>Create a new blog post</h1>
                <hr class="mb-5">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{ route('post-post') }}">
                    @csrf
                    <div class="form-group">
                        <input id="post-title" name="title" class="form-control" type="text" placeholder="Title"
                               required>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" name="body" id="post-body" rows="10" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary d-flex ml-auto">Submit</button>
                </form>
            </div>
        </div>
    </div>
@endsection
