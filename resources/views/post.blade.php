@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="d-flex">
                    <h1>{{$post->title}}</h1>
                    @include('elements.post.control-links')
                </div>
                <h6>Posted {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
                    by {{$post->user->name}} </h6>
                <hr>
                <p>{!! nl2br(e($post->body)) !!}</p>
                <hr class="my-5">
                @include('elements.comment.comments')
            </div>
        </div>
    </div>
@endsection
