@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($posts as $post)
                    <div class="card mb-5">
                        <div class="card-header d-flex">
                            <div>
                                <a href="{{ route('get-post', ['id' => $post->id]) }}">
                                    <div>{{ $post->title }}</div>
                                </a>
                                <div>
                                    <h6>
                                        Posted {{ \Carbon\Carbon::parse($post->created_at)->diffForHumans() }}
                                        by {{ $post->user->name }}
                                    </h6>
                                </div>
                            </div>
                            @include('elements.post.control-links')
                        </div>
                        <div class="card-body">
                            <p>{!! nl2br(e(\Illuminate\Support\Str::limit($post->body, 200, $end='...'))) !!}</p>
                            <a href="{{ route('get-post', ['id' => $post->id]) }}">
                                <button class="btn btn-outline-secondary btn-sm mb-3">Read more</button>
                            </a>
                            <h6>{{ $post->commentCount() }} comments</h6>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
