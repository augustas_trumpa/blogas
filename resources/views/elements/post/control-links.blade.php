@if($post->isOwner())
    <div class="ml-auto">
        <h6 class="ml-auto d-flex">
            <a href="{{ route('get-update-post', ['id' => $post->id]) }}">
                <button class="btn btn-secondary btn-sm mr-2">edit</button>
            </a>
            <form method="POST" action="{{ route('delete-post', ['id' => $post->id]) }}">
                @method('DELETE')
                @csrf
                <button type="submit" class="btn btn-secondary btn-sm">delete</button>
            </form>
        </h6>
    </div>
@endif
