<div class="card mb-3">
    <div class="card-header d-flex">
        <div>
            <h6>{{$comment->author()}}
                commented {{ \Carbon\Carbon::parse($comment->created_at)->diffForHumans() }}</h6>
        </div>
    </div>
    <div class="card-body">
        <p>{!! nl2br(e($comment->body)) !!}</p>
    </div>
</div>
