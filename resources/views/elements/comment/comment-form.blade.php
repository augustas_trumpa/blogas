<h5> Leave a comment </h5>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="POST" action="{{ route('post-comment', ['post' => $post->id]) }}">
    @csrf
    <div class="form-group">
        <textarea id="post-form-body" class="form-control mb-2" name="body" rows="3" required></textarea>

        <button type="submit" class="btn btn-primary d-flex ml-auto">Submit</button>
    </div>
</form>

