<h3 class="mb-5"> Comment section </h3>

@foreach($post->comments as $comment)
    @include('elements.comment.comment')
@endforeach
<div class="mt-5">
    @include('elements.comment.comment-form')
</div>
