<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Post extends Model
{
    protected $fillable = ['title', 'body', 'user_id'];

    public function user(): BelongsTo
    {
        return $this->belongsTo('\App\User');
    }

    public function comments(): HasMany
    {
        return $this->hasMany('\App\Comment');
    }

    public function commentCount(): int
    {
        return count($this->comments);
    }

    public function isOwner(): bool
    {
        if (!auth()->check()) {
            return false;
        }

        return $this->user_id === auth()->id();
    }
}
