<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PostController extends Controller
{
    public function index(): View
    {
        $posts = Post::all();
        return view('posts', compact('posts'));
    }

    public function view(int $id): View
    {
        $post = Post::find($id);
        return view('post', compact('post'));
    }

    public function create(Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'title' => "required|max:255",
            'body' => "required",
        ]);

        $post = new Post($validated);
        $post->user_id = auth()->id();
        $post->save();

        return redirect(route('get-posts'));
    }

    public function createView(): View
    {
        return view('create-post');
    }

    public function updateView(int $id): View
    {
        $post = Post::find($id);

        if (!$post->isOwner()) {
            return redirect()->back();
        }

        return view('update-post', compact('post'));
    }

    public function update(Request $request, int $id): RedirectResponse
    {
        $validated = $request->validate([
            'title' => "required|max:255",
            'body' => "required",
        ]);

        /** @var Post $post */
        $post = Post::find($id);
        if (!$post->isOwner()) {
            return redirect()->back();
        }
        $post->fill($validated);
        $post->save();

        return redirect(route('get-posts'));
    }

    public function delete(int $id): RedirectResponse
    {
        $post = Post::find($id);

        if (!$post->isOwner()) {
            return redirect()->back();
        }

        $post->delete();
        return redirect(route('get-posts'));
    }
}
