<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function create(Request $request): RedirectResponse
    {
        $validated = $request->validate([
            'body' => "required",
        ]);

        $comment = new Comment($validated);

        $comment->post_id = $request->get('post');

        if (auth()->check()) {
            $comment->user_id = auth()->id();
        }

        $comment->save();

        return redirect()->back();
    }
}
